# Dynamics of the Solid

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/emile.roux.83%2Fdynamiquedusolide/master)

A collection a Notebooks  to solve Solid dynamics problem using numerical approach in Python.



The following problems are solved :
* Single mass spring and mass spring damper problem 
* modelling of bungee jumping
* Analysis of the simple pendulum - limits of the linearized model

<img src="https://upload.wikimedia.org/wikipedia/commons/6/6f/Pendulum-no-text.gif" width="200" >
